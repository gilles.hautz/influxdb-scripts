HOST=$1
VALUE=`dig www.google.com @$HOST | grep "Query time" | cut -f 4 -d " "`
curl -i -XPOST 'http://localhost:8086/write?db=monitoring' --data-binary "dig_from,host=$HOST value=$VALUE"
echo "$HOST:$VALUE"
