TO=$1
FROM=`hostname -s`
RESULT=`ping -c 1 $TO`
if [ $? == 1 ]
then
	VALUE=-1
else
	VALUE=`echo "$RESULT" | grep "ttl" | cut -f 4 -d "=" | cut -f 1 -d " "`
fi
curl -i -XPOST 'http://localhost:8086/write?db=monitoring' --data-binary "ttl_to,from=$FROM,to=$TO value=$VALUE"
echo "$FROM:$TO:$VALUE"
